import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import { Lander } from "./ui/routes/guest/lander/lander";
import { SignUp } from "./ui/routes/guest/signup/signup";
import { SignIn } from "./ui/routes/guest/signin/signin";
import { ResetPw } from "./ui/routes/guest/resetPw/reset-pw";
import { Welcome } from "./ui/routes/guest/welcome/welcome";
import { Fm } from "./ui/routes/guest/fm/fm";
import { Delivery } from "./ui/routes/authen/delivery/delivery";
import { DeliveryCategoryLayout } from "./ui/routes/authen/delivery-category/delivery-category-layout";

class App extends Component {
  render() {
    return (
      <Router>
        <Route path="/" exact component={Lander} />
        <Route path="/sign-up/" exact component={SignUp} />
        <Route path="/sign-in/" exact component={SignIn} />
        <Route path="/reset-password/" exact component={ResetPw} />
        <Route path="/welcome/" exact component={Welcome} />
        <Route path="/fm/" exact component={Fm} />
        <Route path="/delivery" exact component={Delivery} />
        <Route
          path="/delivery-category/:id"
          component={DeliveryCategoryLayout}
        />
      </Router>
    );
  }
}

export default App;
