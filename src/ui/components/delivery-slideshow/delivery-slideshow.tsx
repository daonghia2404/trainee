import React, { Component, Children } from "react";
import classNames from "classnames";
import "./delivery-slideshow.scss";
import { ArrowRightIcon } from "ui/components/icons/icons";

type Props = {
  backgroundColor?: string;
  number: number;
  title: string;
};

export class DeliverySlideShow extends Component<Props> {
  state = {};

  render() {
    const { backgroundColor, title, number, children } = this.props;
    return (
      <div
        className={classNames("slide-show-item", {
          gray: backgroundColor === "gray"
        })}
      >
        <div className="header">
          <h3>{title}</h3>
          <p>
            View {number} + more <ArrowRightIcon />
          </p>
        </div>
        {children}
      </div>
    );
  }
}
