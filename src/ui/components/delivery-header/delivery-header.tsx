import React, { Component } from "react";
import "./delivery-header.scss";

import Avatar from "assets/images/delivery-avatar.png";
import Logo from "assets/images/delivery-hyperwolf.png";

import { ArrowDownIcon, SearchIcon } from "ui/components/icons/icons";
import { CartIcon } from "ui/components/icons/icons";

import { CloseIcon } from "ui/components/icons/icons";

import { Drawer } from "antd";

export class HeaderDelivery extends Component {
  state = { visible: false, placement: "bottom" as const };

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  onChange = (e: any) => {
    this.setState({
      placement: e.target.value
    });
  };

  render() {
    return (
      <div className="header-delivery">
        <div className="nav">
          <div className="avatar">
            <img src={Avatar} alt="" />
          </div>
          <div className="title">
            <h1>
              HyperWolf <ArrowDownIcon />
            </h1>
            <p>delivery in 90291</p>
          </div>
          <div className="cart">
            <CartIcon />
            <span>0</span>
          </div>
        </div>
        <div className="logo">
          <img src={Logo} alt="" />
          <p onClick={this.showDrawer}>More Info</p>
        </div>
        <div className="search">
          <div className="search__input">
            <SearchIcon />
            <input type="text" placeholder="Search menu" />
          </div>
          <div className="points">
            <span>100</span>points
          </div>
        </div>
        <div>
          <Drawer
            placement={this.state.placement}
            closable={false}
            onClose={this.onClose}
            visible={this.state.visible}
            className="more-info"
          >
            <div style={{ height: 15 + "px" }} onClick={this.onClose}>
              <CloseIcon className="close-icon" />
            </div>
            <div className="logo">
              <img src={Logo} alt="" />
              <h2>Hyper Wolf</h2>
            </div>
            <p>
              Must have valid medical recommendation and california ID. $50 cap
              on flower. Free parking lot in the rear (on South Lincoln).
            </p>
            <h2>Delivery Info</h2>
            <p>
              Delivery fee of $3.99 for orders under $50. Free delivery for
              orders $50 and more.
            </p>
            <h2>Hours</h2>
            <div className="calendar">
              <div className="row">
                <div className="day">Thursday</div>
                <div className="time">10:30am - 10:00pm</div>
              </div>
              <div className="row">
                <div className="day">Friday</div>
                <div className="time">10:30am - 10:00pm</div>
              </div>
              <div className="row">
                <div className="day">Saturday</div>
                <div className="time">10:30am - 10:00pm</div>
              </div>
              <div className="row">
                <div className="day">Sunday</div>
                <div className="time">10:30am - 10:00pm</div>
              </div>
              <div className="row">
                <div className="day">Monday</div>
                <div className="time">10:30am - 10:00pm</div>
              </div>
              <div className="row">
                <div className="day">Tuesday</div>
                <div className="time">10:30am - 10:00pm</div>
              </div>
            </div>
          </Drawer>
        </div>
      </div>
    );
  }
}
