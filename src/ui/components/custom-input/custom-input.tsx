import React, { Component, createRef } from "react";
import classNames from "classnames";
import "./custom-input.scss";
import { CorrectIcon } from "ui/components/icons/icons";
import { IncorrectIcon } from "ui/components/icons/icons";

import { ValidateEmail } from "utils/validation";
import { ValidateBirth } from "utils/validation";
import { ValidatePassword } from "utils/validation";
import { ValidatePhone } from "utils/validation";

type Props = {
  type?: string;
  password?: boolean;
  correct?: boolean;
};

export class CustomInput extends Component<Props> {
  state = {
    active: false,
    isCorrect: false,
    hidden: false,
    value: ""
  };

  private inputElement = createRef<HTMLInputElement>();

  focus = () => {
    this.setState({
      active: true
    });
  };

  validate = () => {
    const { type, children } = this.props;
    const inputValue = this.inputElement.current?.value;
    this.setState({ value: inputValue });

    if (type === "text") {
      if (inputValue === "") {
        this.setState({
          isCorrect: false
        });
      } else {
        this.setState({
          isCorrect: true
        });
      }
    }

    if (type === "password") {
      if (ValidatePassword(String(inputValue))) {
        this.setState({
          isCorrect: true
        });
      } else {
        this.setState({
          isCorrect: false
        });
      }
    }

    if (type === "email") {
      if (ValidateEmail(String(inputValue))) {
        this.setState({
          isCorrect: true,
          message: ""
        });
      } else {
        this.setState({
          isCorrect: false,
          message: "Invalid Email Address"
        });
      }
    }

    if (type === "text" && children === "Birthday") {
      if (ValidateBirth(String(inputValue))) {
        this.setState({
          isCorrect: true
        });
      } else {
        this.setState({
          isCorrect: false
        });
      }
    }

    if (type === "text" && children === "Enter number") {
      if (ValidatePhone(String(inputValue))) {
        this.setState({
          isCorrect: true,
          message: ""
        });
      } else {
        this.setState({
          isCorrect: false,
          message: "Invalid Phone Number"
        });
      }
    }
  };
  showHidePassword = () => {
    this.setState({
      hidden: !this.state.hidden
    });
  };
  render() {
    const { type, children, password } = this.props;
    return (
      <div className={classNames("field", { active: this.state.active })}>
        <label className={classNames({ active: this.state.active })}>
          {children}
        </label>
        <input
          ref={this.inputElement}
          onChange={this.validate}
          onFocus={this.focus}
          type={this.state.hidden ? "text" : type}
        />
        {password && (
          <div onClick={this.showHidePassword} className="showHidePassword">
            SHOW
          </div>
        )}
        {this.state.value !== "" &&
          (this.state.isCorrect ? (
            <CorrectIcon className="correct" />
          ) : (
            <IncorrectIcon className="correct" />
          ))}
      </div>
    );
  }
}
