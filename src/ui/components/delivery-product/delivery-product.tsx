import React, { Component } from "react";
import "./delivery-product.scss";
import { PlusIcon } from "ui/components/icons/icons";

type Props = {
  image: any;
  overlay?: string;
  title: string;
  des: string;
  price: string;
  overlayColor?: string;
};

export class DeliveryProduct extends Component<Props> {
  render() {
    const { image, overlay, title, des, price, overlayColor } = this.props;
    return (
      <div className="delivery-product">
        <div className="product-image">
          <div className="icon">
            <PlusIcon />
          </div>
          <img src={image} alt="" />
          {typeof overlay !== "undefined" && (
            <div style={{ background: overlayColor }} className="overlay">
              {overlay}
            </div>
          )}
        </div>
        <div className="product-content">
          <h4>{title}</h4>
          <p>{des}</p>
          <span>{price}</span>
        </div>
      </div>
    );
  }
}
