import React from "react";
import iconFacebook from "assets/icons/icon-facebook.svg";
import iconFlag from "assets/icons/icon-favicon.svg";

import iconCorrect from "assets/icons/icon-correct.svg";
import iconInCorrect from "assets/icons/icon-incorrect.png";

import iconCarretDown from "assets/icons/icon-carret-down.svg";
import iconArrowLeft from "assets/icons/icon-arrow-left.svg";
import iconArrowRight from "assets/icons/icon-arrow-right.svg";
import iconArrowDown from "assets/icons/icon-arrow-down.svg";
import iconClose from "assets/icons/icon-close.svg";

import iconCart from "assets/icons/icon-cart.svg";
import iconPlus from "assets/icons/icon-plus.svg";
import iconSearch from "assets/icons/icon-search.svg";

interface IconType {
  className?: string;
  onClick?: Function;
  white?: boolean;
}

export const FacebookIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconFacebook} className={className} alt="" />
);

export const PlusIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconPlus} className={className} alt="" />
);

export const ArrowLeftIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconArrowLeft} className={className} alt="" />
);

export const FaviconIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconFlag} className={className} alt="" />
);

export const CorrectIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconCorrect} className={className} alt="" />
);

export const CarretDownIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconCarretDown} className={className} alt="" />
);

export const IncorrectIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconInCorrect} className={className} alt="" />
);

export const ArrowDownIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconArrowDown} className={className} alt="" />
);

export const ArrowRightIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconArrowRight} className={className} alt="" />
);

export const CartIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconCart} className={className} alt="" />
);

export const SearchIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconSearch} className={className} alt="" />
);

export const CloseIcon = ({ className }: IconType): React.ReactElement => (
  <img src={iconClose} className={className} alt="" />
);
