import React, { Component } from "react";
import "./popup-error.scss";
import classNames from "classnames";
type Props = {
  isActive: boolean;
};
export class PopupError extends Component<Props> {
  render() {
    const { children, isActive } = this.props;
    return (
      <div className={classNames("popupError", { active: isActive })}>
        <p>{children}</p>
      </div>
    );
  }
}
