import React from "react";
import "./delivery-ticket.scss";

type Props = {
  sale: string;
  date: string;
  code: string;
};

export const DeliveryTicket = (props: Props) => {
  const { sale, date, code } = props;
  return (
    <div className="ticket">
      <h3>{sale}</h3>
      <p>{date}</p>
      <div className="line"></div>
      <div className="code">
        <img src={code} alt="" />
      </div>
    </div>
  );
};
