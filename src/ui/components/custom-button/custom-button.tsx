import React, { ReactNode } from "react";
import "./custom-button.scss";
import classNames from "classnames";

type Props = {
  className?: string;
  children?: ReactNode;
};

export const Button = (props: Props) => {
  const { className } = props;

  return (
    <div
      className={classNames(
        "custom-button",
        { orange: className === "orange" },
        { blue: className === "blue" }
      )}
    >
      {props.children}
    </div>
  );
};
