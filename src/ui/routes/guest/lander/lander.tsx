import React, { Component } from "react";
import { Button } from "ui/components/custom-button/custom-button";
import { Link } from "react-router-dom";

import "react-alice-carousel/lib/scss/alice-carousel.scss";
import AliceCarousel from "react-alice-carousel";

import "./lander.scss";

import Logo from "assets/images/fm-logo.png";
import Step1 from "assets/images/landing-image-step-1.png";
import Step2 from "assets/images/landing-image-step-2.png";
import Step3 from "assets/images/landing-image-step-3.png";
import Step4 from "assets/images/landing-image-step-4.png";
import { FacebookIcon } from "ui/components/icons/icons";

export class Lander extends Component {
  state = {
    items: [
      {
        urlImage: Step1,
        title: "Cannabis delivery at your fingertips!"
      },
      {
        urlImage: Step2,
        title: "Earn Loyalty from each delivery"
      },
      {
        urlImage: Step3,
        title: "Find where the product you want is"
      },
      {
        urlImage: Step4,
        title: "Chat with us to find the correct item for you"
      }
    ]
  };
  render() {
    const { items } = this.state;
    return (
      <div className="Lander">
        <img className="Lander__logo" src={Logo} alt="" />
        <AliceCarousel
          dotsDisabled
          buttonsDisabled
          autoPlay={true}
          autoPlayInterval={3000}
        >
          {items.map((item, index) => (
            <div key={index} className="Lander__item">
              <h1>{item.title}</h1>
              <div className="Lander__image">
                <img src={item.urlImage} alt="" />
              </div>
            </div>
          ))}
        </AliceCarousel>
        <div className="Lander__bottom">
          <Button className="blue">
            <FacebookIcon />
            Sign Up with facebook
          </Button>
          <Button className="orange">Sign Up with gmail</Button>
          <p>
            Already have an account? <Link to="/sign-in">Login</Link>
          </p>
        </div>
      </div>
    );
  }
}
