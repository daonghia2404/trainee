import React from "react";
import "../welcome/welcome.scss";
import Logo from "assets/images/fm-logo-white.png";

export const Fm = () => {
  return (
    <div className="welcome">
      <img className="Lander__logo" src={Logo} alt="" />
    </div>
  );
};
