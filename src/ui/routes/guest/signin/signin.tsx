import React from "react";
import "../signup/signup.scss";

import { Link } from "react-router-dom";

import { ArrowLeftIcon } from "ui/components/icons/icons";
import { FaviconIcon } from "ui/components/icons/icons";

import "react-alice-carousel/lib/scss/alice-carousel.scss";

import { Button } from "ui/components/custom-button/custom-button";
import { CustomInput } from "ui/components/custom-input/custom-input";

export const SignIn = () => {
  return (
    <div className="SignUp">
      <header>
        <ArrowLeftIcon />
        <Link to="/sign-up">Sign Up</Link>
      </header>
      <div className="SignUp__icon">
        <FaviconIcon />
      </div>
      <form action="#" className="SignUp__items">
        <div className="SignUp__item">
          <h2>Welcome Back! Let’s sign in.</h2>
          <div className="field-input">
            <CustomInput correct={true} type="text">
              Email / Phone
            </CustomInput>
          </div>
          <div className="field-input">
            <CustomInput type="password" password={true}>
              Password
            </CustomInput>
          </div>
          <p className="note">
            <Link to="/reset-password/">Forgot Password?</Link>
          </p>
        </div>
      </form>
      <Button className="orange">Sign In</Button>
    </div>
  );
};
