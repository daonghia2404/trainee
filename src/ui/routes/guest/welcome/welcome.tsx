import React from "react";
import "./welcome.scss";
import { Link } from "react-router-dom";

export const Welcome = () => {
  return (
    <Link to="/fm/">
      <div className="welcome">
        Welcome, <br /> Jacob!
      </div>
    </Link>
  );
};
