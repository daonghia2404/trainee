import React, { Component } from "react";

import { ArrowLeftIcon, CarretDownIcon } from "ui/components/icons/icons";
import { FaviconIcon } from "ui/components/icons/icons";

import { Link } from "react-router-dom";

import "react-alice-carousel/lib/scss/alice-carousel.scss";
import AliceCarousel from "react-alice-carousel";

import { Button } from "ui/components/custom-button/custom-button";
import { CustomInput } from "ui/components/custom-input/custom-input";

import { Drawer } from "antd";
import "antd/dist/antd.css";

import "./signup.scss";

export class SignUp extends Component {
  state = {
    visible: false,
    visible2: false,
    placement: "bottom" as const,
    currentIndex: 0,
    invalid: true,
    messageError: ""
  };

  slideNext = () => {
    if (this.state.currentIndex === 5) return;
    this.setState({ currentIndex: this.state.currentIndex + 1 });
  };

  slidePrev = () => {
    if (this.state.currentIndex === 0) return;
    this.setState({ currentIndex: this.state.currentIndex - 1 });
  };

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };
  showDrawer2 = () => {
    this.setState({
      visible2: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
      visible2: false
    });
  };

  showInvalidMsg = () => {
    this.setState({ invalid: true });
  };

  render() {
    return (
      <div className="SignUp">
        <header onClick={() => this.slidePrev()}>
          <ArrowLeftIcon />
        </header>
        <div className="SignUp__icon">
          <FaviconIcon />
        </div>
        <form action="#" className="SignUp__items">
          <AliceCarousel
            dotsDisabled
            buttonsDisabled
            touchTrackingEnabled={false}
            infinite={false}
            slideToIndex={this.state.currentIndex}
          >
            {/* Start Name */}
            <div className="SignUp__item">
              <h2>Hey! What should we call you?</h2>
              <div className="field-input">
                <CustomInput type="text">First Name</CustomInput>
              </div>
              <div className="field-input">
                <CustomInput type="text">Last Name</CustomInput>
              </div>
            </div>
            {/* End Name */}

            {/* Start Phone Number */}
            <div className="SignUp__item">
              <h2>What's your phone number, Jacob?</h2>
              <div className="field-input">
                <select id="phone">
                  <option value="+1">+1</option>
                  <option value="+1">+84</option>
                  <option value="+1">+123</option>
                </select>
                <CarretDownIcon className="carretDown" />
                <CustomInput type="text">Enter number</CustomInput>
              </div>
              <p className="note">
                A 4 digit OTP will be sent via SMS to verify your mobile number.
              </p>
            </div>
            {/* End Phone Number */}

            {/* Start Ver Code */}
            <div className="SignUp__item">
              <h2>Let’s check that verification code!</h2>
              <p className="note">
                Enter the 4 digit code send to{" "}
                <span style={{ textDecoration: "none" }}>+1(414) 323-8402</span>
              </p>
              <div
                className="field-input"
                style={{ borderBottom: "none", margin: `${30}px ${0}` }}
              >
                <input
                  id="verCode1"
                  min="0"
                  max="9"
                  type="number"
                  className="verCode"
                />
                <input
                  id="verCode2"
                  min="0"
                  max="9"
                  type="number"
                  className="verCode"
                />
                <input
                  id="verCode3"
                  min="0"
                  max="9"
                  type="number"
                  className="verCode"
                />
                <input
                  id="verCode4"
                  min="0"
                  max="9"
                  type="number"
                  className="verCode"
                />
              </div>
              <p className="note" style={{ textDecoration: "underline" }}>
                Resend code in 4:33
              </p>
            </div>
            {/* End Ver Code */}

            {/* Start Password */}
            <div className="SignUp__item">
              <h2>Great! Let’s set your password.</h2>
              <div className="field-input">
                <CustomInput type="password" password={true}>
                  Password
                </CustomInput>
              </div>
            </div>
            {/* End Password */}

            {/* Start Emali */}
            <div className="SignUp__item">
              <h2>And, your email address?</h2>
              <div className="field-input">
                <CustomInput type="email">Email Address</CustomInput>
              </div>
            </div>
            {/* End Emali */}

            {/* Start Birthday */}
            <div className="SignUp__item">
              <h2>Last thing! When is your birthday?</h2>
              <div className="field-input">
                <CustomInput type="text">Birthday</CustomInput>
              </div>
              <p className="note">
                By signing up, I agree to Finding Mary’s{" "}
                <span onClick={this.showDrawer}>Terms of Service</span> and{" "}
                <span onClick={this.showDrawer2}>Privacy Policy</span>.
              </p>
            </div>
            {/* End Birthday */}
          </AliceCarousel>
        </form>
        <div onClick={() => this.slideNext()}>
          <Button className="orange">
            {this.state.currentIndex >= 5 ? (
              <Link style={{ color: "#fff" }} to="/welcome/">
                Next
              </Link>
            ) : (
              "Next"
            )}
          </Button>
        </div>

        <Drawer
          placement={this.state.placement}
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
        >
          <div onClick={this.onClose}>
            <ArrowLeftIcon />
          </div>
          <h3>Terms & Conditions</h3>
          <p>Last Updated January 1, 2017</p>
          <p>
            Welcome to the service provided by Uniform LLC. (“Second Screen”,
            "we", "us" and/or "our"). This privacy policy has been created to
            provide information about our company, our website and the services
            we provide (including through our mobile applications)
            (collectively, the "Services") to our users ("you" and/or "your").
            This Privacy Policy sets forth Second Screen’s policy with respect
            to information, including personally identifiable data ("Personal
            Data"), that is collected from users of the Services.
          </p>
          <p>Information We Collect:</p>
          <p>
            When you interact with us through the Services, we may collect
            Personal Data and other information from you, as further described
            below:
          </p>
          <p>
            Personal Data That You Provide Through the Services: We collect
            Personal Data from you when you voluntarily provide such
            information, such as when you register for access to our Services
            and/or third party services (for example, username and email address
            provided by services such as Facebook Connect), provide us with
            credit card and other payment information, use certain Services
            including withdrawal of funds, contact us with inquiries or respond
            to one of our surveys. Wherever Uniform collects Personal Data we
            make an effort to provide a link to this Privacy Policy.
          </p>
        </Drawer>

        <Drawer
          placement={this.state.placement}
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible2}
        >
          <div onClick={this.onClose}>
            <ArrowLeftIcon />
          </div>
          <h3>Privacy Policy</h3>
          <p>Last Updated January 1, 2017</p>
          <p>
            Welcome to the service provided by Uniform LLC. (“Second Screen”,
            "we", "us" and/or "our"). This privacy policy has been created to
            provide information about our company, our website and the services
            we provide (including through our mobile applications)
            (collectively, the "Services") to our users ("you" and/or "your").
            This Privacy Policy sets forth Second Screen’s policy with respect
            to information, including personally identifiable data ("Personal
            Data"), that is collected from users of the Services.
          </p>
          <p>Information We Collect:</p>
          <p>
            When you interact with us through the Services, we may collect
            Personal Data and other information from you, as further described
            below:
          </p>
          <p>
            Personal Data That You Provide Through the Services: We collect
            Personal Data from you when you voluntarily provide such
            information, such as when you register for access to our Services
            and/or third party services (for example, username and email address
            provided by services such as Facebook Connect), provide us with
            credit card and other payment information, use certain Services
            including withdrawal of funds, contact us with inquiries or respond
            to one of our surveys. Wherever Uniform collects Personal Data we
            make an effort to provide a link to this Privacy Policy.
          </p>
        </Drawer>
      </div>
    );
  }
}
