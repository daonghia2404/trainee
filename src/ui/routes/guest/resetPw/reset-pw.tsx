import React, { Component } from "react";
import "../signup/signup.scss";

import { Link } from "react-router-dom";

import { ArrowLeftIcon, CarretDownIcon } from "ui/components/icons/icons";

import "react-alice-carousel/lib/scss/alice-carousel.scss";
import AliceCarousel from "react-alice-carousel";

import { Button } from "ui/components/custom-button/custom-button";
import { CustomInput } from "ui/components/custom-input/custom-input";

import "./reset-pw.scss";

export class ResetPw extends Component {
  state = {
    currentIndex: 0
  };
  slideNext = () => {
    if (this.state.currentIndex === 5) return;
    this.setState({ currentIndex: this.state.currentIndex + 1 });
  };

  slidePrev = () => {
    if (this.state.currentIndex === 0) return;
    this.setState({ currentIndex: this.state.currentIndex - 1 });
  };
  render() {
    return (
      <div className="SignUp">
        <header onClick={() => this.slidePrev()}>
          <ArrowLeftIcon />
        </header>
        <form action="#" className="SignUp__items">
          <AliceCarousel
            slideToIndex={this.state.currentIndex}
            dotsDisabled
            buttonsDisabled
            touchTrackingEnabled={false}
            infinite={false}
          >
            {/* Start Password step 1 */}
            <div className="SignUp__item">
              <h2>Reset your password via phone number</h2>
              <p className="note">
                You will be sent a text with instructions to reset your
                password.
              </p>
              <div className="field-input">
                <select id="phone">
                  <option value="+1">+1</option>
                  <option value="+1">+84</option>
                  <option value="+1">+123</option>
                </select>
                <CarretDownIcon className="carretDown" />
                <CustomInput type="text">Enter number</CustomInput>
              </div>
            </div>
            {/* End Password step 1 */}

            {/* Start Password step 2 */}
            <div className="SignUp__item">
              <h2>Verify to Reset Password</h2>
              <p className="note">
                Enter the 4 digit code send to +1(414) 323-8402
              </p>
              <div
                className="field-input"
                style={{ borderBottom: "none", margin: `${30}px ${0}` }}
              >
                <input id="verCode1" type="text" className="verCode" />
                <input id="verCode2" type="text" className="verCode" />
                <input id="verCode3" type="text" className="verCode" />
                <input id="verCode4" type="text" className="verCode" />
              </div>
              <p className="note" style={{ textDecoration: "underline" }}>
                Resend code in 4:33
              </p>
            </div>
            {/* End Password step 2 */}

            {/* Start Password step 3 */}
            <div className="SignUp__item">
              <h2>Verified! Let’s set your new password.</h2>
              <div className="field-input" style={{ marginTop: 70 + "px" }}>
                <CustomInput correct={true} type="password" password={true}>
                  Password
                </CustomInput>
              </div>
            </div>
            {/* End Password step 3 */}

            {/* Start Password step 3 */}
            <div className="SignUp__item final">
              <h2>Verified! Let’s set your new password.</h2>
              <Link to="/sign-in">
                <Button className="orange">Take me to Sign In</Button>
              </Link>
            </div>
            {/* End Password step 3 */}
          </AliceCarousel>
        </form>
        {this.state.currentIndex < 3 && (
          <div onClick={() => this.slideNext()}>
            <Button className="orange">Next</Button>
          </div>
        )}
      </div>
    );
  }
}
