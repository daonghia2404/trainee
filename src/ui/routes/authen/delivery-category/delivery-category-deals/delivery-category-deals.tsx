import React, { Component } from "react";
import { DeliveryTicket } from "ui/components/delivery-ticket/delivery-ticket";
import Code from "assets/images/delivery-code.png";
import "./delivery-category-deals.scss";
interface Props {}
interface State {}

export class DeliveryCategoryDeals extends Component<Props, State> {
  state = {
    ticket: [
      {
        sale: "20% off orders of $100+",
        date: "Happy Hour | Wed 1-2pm",
        code: Code
      },
      {
        sale: "20% off orders of $100+",
        date: "Happy Hour | Wed 1-2pm",
        code: Code
      },
      {
        sale: "20% off orders of $100+",
        date: "Happy Hour | Wed 1-2pm",
        code: Code
      }
    ]
  };

  render() {
    const { ticket } = this.state;
    return (
      <div className="delivery-category-deals">
        <div className="delivery-category-tickets">
          {ticket.length > 0 &&
            ticket.map((item, index) => (
              <DeliveryTicket
                key={index}
                sale={item.sale}
                date={item.date}
                code={item.code}
              />
            ))}
        </div>
      </div>
    );
  }
}
