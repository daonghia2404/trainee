import React, { Component } from "react";

import "./delivery-category-rewards.scss";

import Product1 from "assets/images/delivery-product-1.png";
import Product2 from "assets/images/delivery-product-2.png";
import Product3 from "assets/images/delivery-product-3.png";
import Product4 from "assets/images/delivery-product-4.png";

import { DeliveryProduct } from "ui/components/delivery-product/delivery-product";

export class DeliveryCategoryRewards extends Component {
  state = {
    products: [
      {
        image: Product2,
        title: "Free 1/8 from the shelf ",
        des: "Reward",
        price: "100 points"
      },
      {
        image: Product1,
        title: "Kiva Terra Bites - Espresso Dark - 2ct",
        des: "Reward",
        price: "250 points"
      },
      {
        image: Product3,
        title: "Kiva Terra Bites - Espresso Dark - 2ct",
        des: "Reward",
        price: "250 points"
      }
    ]
  };

  render() {
    const { products } = this.state;
    return (
      <div className="delivery-category-rewards">
        <div className="current-point">253 points</div>
        <h2 className="title">100 points</h2>
        <div className="delivery-category-products">
          {products.length > 0 &&
            products.map((item, index) => (
              <DeliveryProduct
                key={index}
                image={item.image}
                title={item.title}
                des={item.des}
                price={item.price}
              />
            ))}
        </div>

        <h2 className="title">250 points</h2>
        <div className="delivery-category-products">
          {products.length > 0 &&
            products.map((item, index) => (
              <DeliveryProduct
                key={index}
                image={item.image}
                title={item.title}
                des={item.des}
                price={item.price}
              />
            ))}
        </div>
      </div>
    );
  }
}
