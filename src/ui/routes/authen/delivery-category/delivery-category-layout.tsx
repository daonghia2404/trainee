import React from "react";
import { Link, useParams } from "react-router-dom";

import { ArrowLeftIcon } from "ui/components/icons/icons";
import Logo from "assets/images/delivery-hyperwolf.png";
import { CartIcon } from "ui/components/icons/icons";
import { SearchIcon } from "ui/components/icons/icons";

import { DeliveryCategoryMenu } from "./delivery-category-menu/delivery-category-menu";
import { DeliveryCategoryDeals } from "./delivery-category-deals/delivery-category-deals";
import { DeliveryCategoryRewards } from "./delivery-category-rewards/delivery-category-rewards";

import classNames from "classnames";
import "./delivery-category-layout.scss";

export const DeliveryCategoryLayout = () => {
  let { id } = useParams();
  return (
    <div
      className={classNames("delivery-category-layout", {
        short: id !== "menu"
      })}
    >
      <div className="header">
        <div className="header__logo">
          <ArrowLeftIcon />
          <img className="logo" src={Logo} alt="" />
          <h1>HyperWolf</h1>
        </div>
        <div className="cart">
          <CartIcon />
          <span>2</span>
        </div>
        <div className="tab">
          <Link
            to="/delivery-category/menu"
            className={classNames("tab__item", { active: id === "menu" })}
          >
            Menu
          </Link>
          <Link
            to="/delivery-category/rewards"
            className={classNames("tab__item", { active: id === "rewards" })}
          >
            Rewards
          </Link>
          <Link
            to="/delivery-category/deals"
            className={classNames("tab__item", { active: id === "deals" })}
          >
            Deals
          </Link>
          <SearchIcon className="search-icon" />
        </div>
      </div>
      {id === "menu" && <DeliveryCategoryMenu />}
      {id === "rewards" && <DeliveryCategoryDeals />}
      {id === "deals" && <DeliveryCategoryRewards />}
    </div>
  );
};
