import React, { Component } from "react";

import "./delivery-category-menu.scss";

import Product1 from "assets/images/delivery-product-1.png";
import Product2 from "assets/images/delivery-product-2.png";
import Product3 from "assets/images/delivery-product-3.png";
import Product4 from "assets/images/delivery-product-4.png";

import { DeliveryProduct } from "ui/components/delivery-product/delivery-product";

export class DeliveryCategoryMenu extends Component {
  private flower: any;
  private edible: any;
  private concentrates: any;
  private vaporizers: any;

  constructor(props: any) {
    super(props);
    this.flower = React.createRef();
    this.edible = React.createRef();
    this.concentrates = React.createRef();
    this.vaporizers = React.createRef();
  }
  scrollToFlower = () =>
    window.scrollTo(0, this.flower.current.offsetTop - 214);
  scrollToEdible = () =>
    window.scrollTo(0, this.edible.current.offsetTop - 214);
  scrollToConcentrates = () =>
    window.scrollTo(0, this.concentrates.current.offsetTop - 214);
  scrollToVaporizers = () =>
    window.scrollTo(0, this.vaporizers.current.offsetTop - 214);
  state = {
    products: [
      {
        image: Product2,
        overlay: "Sativa",
        overlayColor: "#F7BE33",
        title: "Crescendo",
        des: "THC Designs",
        price: "$50 / 3.5g"
      },
      {
        image: Product1,
        overlay: "Hybrid",
        overlayColor: "#006052",
        title: "Releaf 1:3 CBD:THC - MCT Oil - 15ml",
        des: "Dosist",
        price: "$30 / 2.5g"
      },
      {
        image: Product3,
        overlay: "Hybrid",
        overlayColor: "#006052",
        title: "Classic Kingsize S...",
        des: "Raw",
        price: "$2"
      },
      {
        image: Product4,
        overlay: "Indica",
        overlayColor: "#30359C",
        title: "Crescendo",
        des: "THC Designs",
        price: "$15"
      }
    ]
  };
  render() {
    const { products } = this.state;
    return (
      <div className="delivery-category-menu">
        <div className="tab">
          <a href="#flower" onClick={this.scrollToFlower}>
            Flower
          </a>
          <a href="#edible" onClick={this.scrollToEdible}>
            Edible
          </a>
          <a href="#concentrates" onClick={this.scrollToConcentrates}>
            Concentrates
          </a>
          <a href="#vaporizers" onClick={this.scrollToVaporizers}>
            Vaporizers
          </a>
        </div>
        <h2 ref={this.flower} className="title">
          Flower
        </h2>
        <div className="delivery-category-products">
          {products.length > 0 &&
            products.map((item, index) => (
              <DeliveryProduct
                key={index}
                image={item.image}
                overlayColor={item.overlayColor}
                overlay={item.overlay}
                title={item.title}
                des={item.des}
                price={item.price}
              />
            ))}
        </div>

        <h2 ref={this.edible} className="title">
          Edible
        </h2>
        <div className="delivery-category-products">
          {products.length > 0 &&
            products.map((item, index) => (
              <DeliveryProduct
                key={index}
                image={item.image}
                overlayColor={item.overlayColor}
                overlay={item.overlay}
                title={item.title}
                des={item.des}
                price={item.price}
              />
            ))}
        </div>

        <h2 ref={this.concentrates} className="title">
          Concentrates
        </h2>
        <div className="delivery-category-products">
          {products.length > 0 &&
            products.map((item, index) => (
              <DeliveryProduct
                key={index}
                image={item.image}
                overlayColor={item.overlayColor}
                overlay={item.overlay}
                title={item.title}
                des={item.des}
                price={item.price}
              />
            ))}
        </div>

        <h2 ref={this.vaporizers} className="title">
          Vaporizers
        </h2>
        <div className="delivery-category-products">
          {products.length > 0 &&
            products.map((item, index) => (
              <DeliveryProduct
                key={index}
                image={item.image}
                overlayColor={item.overlayColor}
                overlay={item.overlay}
                title={item.title}
                des={item.des}
                price={item.price}
              />
            ))}
        </div>
      </div>
    );
  }
}
