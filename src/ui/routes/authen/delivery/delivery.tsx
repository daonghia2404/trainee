import React, { Component } from "react";

import { HeaderDelivery } from "ui/components/delivery-header/delivery-header";
import { DeliverySlideShow } from "ui/components/delivery-slideshow/delivery-slideshow";
import { DeliveryTicket } from "ui/components/delivery-ticket/delivery-ticket";
import { DeliveryProduct } from "ui/components/delivery-product/delivery-product";

import AliceCarousel from "react-alice-carousel";

import Code from "assets/images/delivery-code.png";
import Product1 from "assets/images/delivery-product-1.png";
import Product2 from "assets/images/delivery-product-2.png";
import Product3 from "assets/images/delivery-product-3.png";
import Product4 from "assets/images/delivery-product-4.png";

export class Delivery extends Component {
  state = {
    ticket: [
      {
        sale: "20% off orders of $100+",
        date: "Happy Hour | Wed 1-2pm",
        code: Code
      },
      {
        sale: "20% off orders of $100+",
        date: "Happy Hour | Wed 1-2pm",
        code: Code
      }
    ],
    products: [
      {
        image: Product2,
        overlay: "Sativa",
        overlayColor: "#F7BE33",
        title: "Crescendo",
        des: "THC Designs",
        price: "$50 / 3.5g"
      },
      {
        image: Product1,
        overlay: "Hybrid",
        overlayColor: "#006052",
        title: "Releaf 1:3 CBD:THC - MCT Oil - 15ml",
        des: "Dosist",
        price: "$30 / 2.5g"
      },
      {
        image: Product3,
        overlay: "Hybrid",
        overlayColor: "#006052",
        title: "Classic Kingsize S...",
        des: "Raw",
        price: "$2"
      },
      {
        image: Product4,
        overlay: "Indica",
        overlayColor: "#30359C",
        title: "Crescendo",
        des: "THC Designs",
        price: "$15"
      }
    ]
  };

  render() {
    const { ticket, products } = this.state;
    return (
      <div className="delivery">
        <HeaderDelivery />
        <DeliverySlideShow backgroundColor="gray" number={15} title="Our Deals">
          <AliceCarousel buttonsDisabled dotsDisabled>
            {ticket.length > 0 &&
              ticket.map((item, index) => (
                <DeliveryTicket
                  key={index}
                  sale={item.sale}
                  date={item.date}
                  code={item.code}
                />
              ))}
          </AliceCarousel>
        </DeliverySlideShow>

        <DeliverySlideShow number={10} title="Your Items">
          <AliceCarousel
            buttonsDisabled
            dotsDisabled
            responsive={{ 0: { items: 2 } }}
          >
            {products.length > 0 &&
              products.map((item, index) => (
                <DeliveryProduct
                  key={index}
                  image={item.image}
                  overlayColor={item.overlayColor}
                  overlay={item.overlay}
                  title={item.title}
                  des={item.des}
                  price={item.price}
                />
              ))}
          </AliceCarousel>
        </DeliverySlideShow>

        <DeliverySlideShow number={10} title="Best Seller">
          <AliceCarousel
            buttonsDisabled
            dotsDisabled
            responsive={{ 0: { items: 2 } }}
          >
            {products.length > 0 &&
              products.map((item, index) => (
                <DeliveryProduct
                  key={index}
                  image={item.image}
                  overlayColor={item.overlayColor}
                  overlay={item.overlay}
                  title={item.title}
                  des={item.des}
                  price={item.price}
                />
              ))}
          </AliceCarousel>
        </DeliverySlideShow>

        <DeliverySlideShow number={10} title="Rewards">
          <AliceCarousel
            buttonsDisabled
            dotsDisabled
            responsive={{ 0: { items: 2 } }}
          >
            {products.length > 0 &&
              products.map((item, index) => (
                <DeliveryProduct
                  key={index}
                  image={item.image}
                  overlayColor={item.overlayColor}
                  overlay={item.overlay}
                  title={item.title}
                  des={item.des}
                  price={item.price}
                />
              ))}
          </AliceCarousel>
        </DeliverySlideShow>
      </div>
    );
  }
}
